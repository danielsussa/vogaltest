/**
 * Created by daniel on 27/08/16.
 */
public interface Stream {
    public char getNext();
    public boolean hasNext();
}
