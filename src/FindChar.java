import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by daniel on 27/08/16.
 */

public class FindChar implements Stream{
    private String value;
    private int index=0;
    private final List<Character> vogalList = Arrays.asList('a','á','ã','e','é','i','í','o','ó','u','ú');

    public FindChar(String value){
        this.value = value.toLowerCase();
    }

    public void setChar(String value){
        this.value = value;
    }

    public char getNext(){
        return value.charAt(index++);
    }

    public boolean hasNext(){
        if(index<value.length()){
            return true;
        }
        index = 0;
        return false;
    }

    public String firstChar(){
        //A lista vogalListCopy é uma cópia da lista vogalList, que contém todas as vogais
        //A lista findedChars adicionará as vogais encontradas na String
        List<Character> vogalListCopy = new ArrayList<>(vogalList);
        List<Character> findedChars = new ArrayList<>();

        //Loop enquanto houver caracteres
        while(hasNext()){
            char ch = getNext();
            if(vogalListCopy.contains(ch)){
                if(!findedChars.contains(ch)){
                    //Adicionamos o registro caso seja uma vogal e não esteja na lista
                    findedChars.add(ch);
                }else{
                    //Se esta vogal aparecer pela segunda vez, exclua ela da vogalListCopy e da findedChars
                    //Esta vogal está excluída totalmente dos registros
                    vogalListCopy.remove(vogalListCopy.indexOf(ch));
                    findedChars.remove(findedChars.indexOf(ch));
                }
            }
        }

        if(findedChars.size()>0){
            return String.format("O '%s' foi a primeira vogal que encontramos e que não se repete!",findedChars.get(0).toString());
        }else{
            return "Nesta palavra não encontramos nenhuma vogal que não se repete";
        }
    }
}
